#!/usr/bin/python
import selectors
import socket
from builtins import str
import sys
import time


from chess.session import Session
from chess.utils import wrap, CREATE_SESSION, GET_SESSIONS, GET_GAME_STATE, MAKE_MOVE, \
	GET_MOVES, JOIN_SESSION, unwrap, recv_msg2, send_msg2, wrap_error, CHAT, RAGEQUIT, opposite_col

HOST = '0.0.0.0'

RESET = False;
GAME_OVER = False


class GameServer(object):
	def __init__(self):
		self.temp_session_name = 'Underground Chess Club'
		self.sessions = {}

	def _generate_access_key(self, remove_later):
		return remove_later

	def process(self, message):
		global GAME_OVER
		global RESET
		# print("raw_message:",message)
		
		if 'message_type' not in message or 'payload' not in message:
			return wrap_error("Malformed message")
   
		msg_type = message['message_type']   
		# print(msg_type)
		# SESSION MANAGEMENT
		if msg_type == CREATE_SESSION:
			desired_color = message['payload']['desired_color']
			self.sessions[self.temp_session_name] = \
				{'session': Session(), 'access_keys': {'white': self._generate_access_key("white_player"),
													   'black': self._generate_access_key("black_player")}}
			white_key = {'name': self.temp_session_name,
						 "access_key": self.sessions[self.temp_session_name]['access_keys']['white']}
			self.sessions[self.temp_session_name]["session"].player_names[0] = message['payload']["user"]
			print("CREATED_SESSION " + str(self.sessions))
			return wrap(CREATE_SESSION, white_key)
		elif msg_type == GET_SESSIONS:
			print("sessions is " + str(self.sessions))
			return wrap(GET_SESSIONS, {'sessions': list(dict.keys(self.sessions))})

		if msg_type == JOIN_SESSION:
			join_name = message['payload']['name']
				
			# CHECK if player belongs to session, or there is free space
			user = message['payload']['user']
			if user in self.sessions[join_name]["session"].player_names:
				if self.sessions[join_name]["session"].player_names[0] == user:
					key = {"name": join_name, "access_key": self.sessions[join_name]['access_keys']['white']}
				elif self.sessions[join_name]["session"].player_names[1] == user:
					key = {"name": join_name, "access_key": self.sessions[join_name]['access_keys']['black']}		
			elif None in self.sessions[join_name]["session"].player_names:
				self.sessions[join_name]["session"].player_names[1] = user;
				key = {"name": join_name, "access_key": self.sessions[join_name]['access_keys']['black']}
			else:
				return wrap_error("You are not welcome here. Be gone.")
			return wrap(JOIN_SESSION, key)

		name = message['payload']['session']['name']
		if name not in self.sessions:
			return wrap_error("Session name does not exist")
	
		current_session = self.sessions[name]
		provided_access_key = message['payload']['session']['access_key']
		if provided_access_key == current_session['access_keys']['white']:
			player_color = "white"
		elif provided_access_key == current_session['access_keys']['black']:
			player_color = "black"
		else:
			return wrap_error("Provided access key is invalid")

		# GAME MANAGEMENT
		if msg_type == GET_GAME_STATE:
			if GAME_OVER:
				RESET = True
				return wrap(RAGEQUIT, current_session['session'].get_game_state(player_color))
			else:
				return wrap(GET_GAME_STATE, current_session['session'].get_game_state(player_color))
		elif msg_type == GET_MOVES:
			moves = current_session['session'].get_moves(player_color, message['payload']['piece_coords'])
			# print(moves)
			if not moves:
				return wrap_error("No possible moves.")
			else:
				return wrap(GET_MOVES, moves)
		elif msg_type == MAKE_MOVE:
			if not current_session['session'].make_move(player_color, message['payload']['desired_move']):
				return wrap_error("Not your turn")
			else:
				return wrap(MAKE_MOVE, current_session['session'].get_game_state(player_color))
		elif msg_type == RAGEQUIT:
			GAME_OVER = True;
			return wrap_error("Bye Bye")
		else:
			return wrap_error("Undefined message type")


def main():
	global GAME_OVER
	global RESET
	
	if len(sys.argv) > 1:
		LISTENING_SOCKET_PORT = int(sys.argv[1])
	else:
		LISTENING_SOCKET_PORT = 9889
		
	game_server = GameServer()

	sel = selectors.DefaultSelector()

	def accept(sock , mask):
		conn, addr = sock.accept()  # Should be ready
		print('Accepted', conn, 'from', addr)
		conn.setblocking(False)
		sel.register(conn, selectors.EVENT_READ, read)

	def read(conn, mask):
		try:
			data = recv_msg2(conn)  # Should be ready
		except:
			return
		if data:
			try:
				to_send = game_server.process(unwrap(data))
				send_msg2(conn, to_send)
			except:
				return
		else:
			sel.unregister(conn)
			conn.close()

	sock = socket.socket()
	sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	sock.bind((HOST, LISTENING_SOCKET_PORT))
	sock.listen(100)
	sock.setblocking(False)
	sel.register(sock, selectors.EVENT_READ, accept)

	
	try:
		last_event = time.time()
		while True:
			events = sel.select(1)
			for key, mask in events:
				callback = key.data
				callback(key.fileobj, mask)
				last_event = time.time()
			
			now = time.time()
			if now - last_event > 24*60*60: # reset server if nothing happens for 24h
				print("SERVER IDLE, RESETTING")
				game_server = GameServer()
				last_event = now;
			
			if RESET:
				print("RESETTING")
				game_server = GameServer()
				last_event = now;	
				GAME_OVER = False;
				RESET = False
				
	except KeyboardInterrupt:
		print("KeyboardInterrupt received. Shutting down.")


if __name__ == "__main__":
	main()
